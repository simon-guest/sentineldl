# Introduction

sentineldl is a wrapper around Sentinelsat (https://pypi.python.org/pypi/sentinelsat) for automatically download Sentinel 2 data from Copernicus Open Access Hub (https://scihub.copernicus.eu). The focus lies on automatically download certain regions in an unattended way. sentineldl also includes scripts for converting the downloaded tiles into GeoTIFFs.

# Installing depencencies

On debian buster, the following packages are required:

``` bash
apt install \
    geomet \
    gdal-bin\
    make \
    parallel \
    sentinelsat \
    unzip
```

## Manual installation of dependencies

The following packages are dependencies for sentinelsat on Debian 10 (buster):

``` bash
apt install \
    python3-configobj \
    python3-scipy \
    python3-geojson \
    python3-wheel \
    python3-html2text \
    python3-requests \
    python3-tqdm \
    python3-click \
    python3-pandas
```

The following packages are required to install sentinelsat via pip:

``` bash
apt install \
    python3-pip \
    python3-setuptools
```

The following commands install sentinelsat:

```bash
pip3 install --no-deps geomet
pip3 install --no-deps sentinelsat
```

Sentineldl and its scripts following dependencies:

``` bash
apt install \
    gdal-bin \
    unzip \
    python3-pandas \
    parallel
```
# Installing sentinelsat

To install sentineldl use the makefile provided. The default location for downloaded data is /var/lib/sentineldl but this can be configured during installation. Example: To download the satellite data to /srv/Sentinel2 use:

``` bash
sudo make install DOWNLOADDIR=/srv/Sentinel2
```

But it is recommended to symlink the DOWNLOADDIR to /var/lib/sentineldl.

# Uninstall

``` bash
sudo make uninstall DOWNLOADDIR=/srv/Sentinel2
```

You may have to clean up /etc/sentineldl and DOWNLOADDIR yourself.
