# Makefile

# Copyright (C) 2018-2020 Simon Spöhel
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

SHELL := sh -e

SOFTWARE = sentineldl
DOWNLOADDIR = /var/lib/$(SOFTWARE)

SCRIPTS = data/*/orig/processing.sh data/*/orig/processing.d/*

test:

	@echo -n "Checking for syntax errors"

	@for SCRIPT in $(SCRIPTS); \
	do \
		sh -n $${SCRIPT}; \
		echo -n "."; \
	done

	@echo " done."

install:

	mkdir -p $(DESTDIR)/etc/$(SOFTWARE)
	cp -r settings/* $(DESTDIR)/etc/$(SOFTWARE)

	mkdir -p $(DESTDIR)/usr/lib/$(SOFTWARE)
	cp -r lib/sentinel $(DESTDIR)/usr/lib/$(SOFTWARE)
	cp lib/sentineldl $(DESTDIR)/usr/lib/$(SOFTWARE)

	mkdir -p $(DESTDIR)/usr/bin
	cp $(SOFTWARE) $(DESTDIR)/usr/bin

	mkdir -p $(DESTDIR)/$(DOWNLOADDIR)
	cp -r data/* $(DESTDIR)/$(DOWNLOADDIR)


uninstall:

	for FILE in settings/*; \
	do \
		rm -f $(DESTDIR)/etc/$(SOFTWARE)/$$(basename $${FILE}); \
	done
	rmdir --ignore-fail-on-non-empty --parents $(DESTDIR)/etc/$(SOFTWARE) || true

	for FILE in lib/sentinel/*; \
	do \
		rm -f $(DESTDIR)/usr/lib/$(SOFTWARE)/sentinel/$$(basename $${FILE}); \
	done
	rm -rf $(DESTDIR)/usr/lib/$(SOFTWARE)/sentinel
	rm -f $(DESTDIR)/usr/lib/$(SOFTWARE)/sentineldl
	rmdir $(DESTDIR)/usr/lib/$(SOFTWARE)

	rm -f $(DESTDIR)/usr/bin/$(SOFTWARE)

	rm -f $(DESTDIR)/$(DOWNLOADDIR)/S2MSI1C/orig/processing.sh
	for FILE in data/S2MSI1C/orig/processing.d/*; \
	do \
		rm -f $(DESTDIR)/$(DOWNLOADDIR)/S2MSI1C/orig/$$(basename $${FILE}); \
	done

	rm -f $(DESTDIR)/$(DOWNLOADDIR)/S2MSI2A/orig/processing.sh
	for FILE in data/S2MSI2A/orig/processing.d/*; \
	do \
		rm -f $(DESTDIR)/$(DOWNLOADDIR)/S2MSI2A/orig/$$(basename $${FILE}); \
	done

	rmdir --ignore-fail-on-non-empty --parents $(DESTDIR)/$(DOWNLOADDIR)/S2MSI1C/orig || true
	rmdir --ignore-fail-on-non-empty --parents $(DESTDIR)/$(DOWNLOADDIR)/S2MSI2A/orig || true

distclean:

reinstall: uninstall install
