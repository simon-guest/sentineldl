# Config file for downloading Sentinel 2 data from SciHub using sentinelsat.
#
# sentineldl searches for /etc/sentineldl and settings/ (in this order) and
# assumes the first that exists to hold the configuration files. All files in
# any subfolder that match *.conf are used config files and merged.
#
# Settings may be stored in one file or across several files possibly in sub
# direcotries. Each file has to be a valid ini file and section names have to
# be unique across all files. See:
# https://docs.python.org/3.5/library/configparser.html#supported-ini-file-structure
# Exactly one hierarchy level of sections is allowed.

# A valid configuration must at least contain the sections "global" and
# "credentials". Any section with another name is treated as region for
# downloading data (see below).

# global
# ======
# This section is mandatory.
[global]
	# This section should hold the platformname and the directory where the
	# data should be downloaded to.

	# "platformname" can only have the value "Sentinel-2" at the moment
	# (requirement by sentinelsat).
	platformname = 'Sentinel-2'

	# The following keys configure the download location and storage layout.
	# Data is downloaded to:
	#      'base_dest'/'product_type'/'orig'

	# "base_dest" is the root that sentineldl uses for downlading the data.
	# Within base_dest sentineldl will create the folders necessary to
	# hold the downloaded data. Currently the following layout is used:
	# <base_dest>/<product_type>/<granuleid>/<year>
	# The storage layout cannot be configured.
	#
	# Use the 'data' to run the script without installation.
	base_dest = data

	# see below for the 'product_type'

	# The name of the folder containing the raw downloaded data
	# WARNING: If you change this, you have to manually change the processing
	#          scripts.
	downloaded_files = orig

# credentials
# ===========
# This section is mandatory.

#[credentials]
# 	username = theusername
# 	password = thepassword


# other sections
# ==============
# Other sections may configure a region and a timespan that should be
# downloaded. The section names are arbitrary and not used during execution of
# sentineldl. Valid keys are "clouds_max", "clouds_min", "date_end",
# "date_start", "footprint", "product_type".
#
# Cloud coverage
# --------------
# Percentace of accepted cloud cover 0 - 100
#
# Dates
# -----
# Images between the dates specified will be searched.
# Expects a date formatted as yyyy-mm-dd. date_end also
# accepts a string 'today'. Note that it does not make sense to download data
# before 2016-12-01 using this script as the storage formate was different
# before and does not fit sentineldl's storage system.
#
# footprint
# ---------
# The filename of a geojson file containing a geometry of the region that should
# be downloaded. The path of the file must be relative to the conf-file that
# references it.
#
# The follwing example is a valid json file for downloading one tile:
#{
#"type": "FeatureCollection",
#"crs": { "type": "name", "properties": { "name": "urn:ogc:def:crs:OGC:1.3:CRS84" } },
#"features": [
#{ "type": "Feature", "properties": { }, "geometry": { "type": "Point", "coordinates": [ 9.802151766690342, 46.597270607460956 ] } }
#]
#}
#
# product_type
# ------------
# Possible value are:
# - S2MSI1C  for Level 1C data
# - S2MSI2A for Level 2A data
#
# Complete example
# ----------------
#
# [che]
#	clouds_min   = 0
#	clouds_max   = 100
#	date_start   = 2016-12-01
#	date_end     = today
#	footprint    = che.geojson
#	product_type = S2MSI1C
