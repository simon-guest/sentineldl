#!/bin/sh

# Copyright (C) 2018-2020 Simon Spöhel
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

set -e

# disabled
exit 0

ZIP="${1}"

DIRECTORY="$(dirname ${ZIP} | cut -d/ -f 2-)"
FILE="$(basename ${ZIP} .zip)"

cd "${BASE_DIRECTORY}"

if [ ! -f "GeoTIFF-fmask/${DIRECTORY}/${FILE}-angles.tif.sha512" ]
then
	echo -n "Creating GeoTIFF-fmask angles for ${FILE} "

	mkdir -p "GeoTIFF-fmask/${DIRECTORY}"

	rm -f "GeoTIFF-fmask/${DIRECTORY}/${FILE}-angles.tif"

	# angles files
	/opt/python-fmask/bin/fmask_sentinel2makeAnglesImage.py		\
		-i ../SAFE/${DIRECTORY}/${FILE}.SAFE/GRANULE/L1C*/*.xml	\
	-o "GeoTIFF-fmask/${DIRECTORY}/${FILE}-angles.tif"

	sha512sum "GeoTIFF-fmask/${DIRECTORY}/${FILE}-angles.tif" > "GeoTIFF-fmask/${DIRECTORY}/${FILE}-angles.tif.sha512"

	echo " done."
fi

if [ ! -f "GeoTIFF-fmask/${DIRECTORY}/${FILE}.tif.sha512" ]
then
	echo -n "Creating GeoTIFF-fmask for ${FILE} "

	rm -f "GeoTIFF-fmask/${DIRECTORY}/${FILE}.tif"

	# fmask files
	/opt/python-fmask/bin/fmask_sentinel2Stacked.py			\
		-a ../VRT/${DIRECTORY}/${FILE}.vrt			\
		-z "GeoTIFF-fmask/${DIRECTORY}/${FILE}-angles.tif"	\
	-o "GeoTIFF-fmask/${DIRECTORY}/${FILE}.tif"

	sha512sum "GeoTIFF-fmask/${DIRECTORY}/${FILE}.tif" > "GeoTIFF-fmask/${DIRECTORY}/${FILE}.tif.sha512"

	echo " done."
fi

cd "${OLDPWD}"
