#!/bin/sh

# Copyright (C) 2018-2020 Simon Spöhel
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

set -e

ZIP="${1}"

DIRECTORY="$(dirname ${ZIP} | cut -d/ -f 2-)"
FILE="$(basename ${ZIP} .zip)"

cd "${BASE_DIRECTORY}"

if [ ! -f "GeoTIFF/${DIRECTORY}/${FILE}.tif.ovr.sha512" ]
then
	echo -n "Creating overview for ${FILE} "

	cd "GeoTIFF/${DIRECTORY}"

	rm -f "${FILE}.tif.ovr"

	gdaladdo					\
		-ro					\
		--config COMPRESS_OVERVIEW DEFLATE	\
		--config PREDICTOR_OVERVIEW 2		\
	"${FILE}.tif" 2 4 8 16 32 64 128 256 512 1024

	sha512sum "${FILE}.tif.ovr" > "${FILE}.tif.ovr.sha512"

	echo " done."
fi

cd "${OLDPWD}"
