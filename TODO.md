* write manpage
* document customization wrt/ processing scripts (both upstream and debian pov):
  - how to add a new one
  - how to disable existing ones
* Improve logging: Add cli for verbosity and log to file.
* Convert the footprint to geojson.
* Investigate the possibilities to use sentinelsat for handling the metadata (geopandas?).
* Add a command line interface for all settings.
* Fix setup of the script: Check cli, check settings, (fallback to defaults where possible or stop). Only then start downloading.
* Catch errors in a more sane way.
* Fail gracefully when destination is read-only.
* Handle processing of data published before 2016-12-06.
* Gracefully exit on Ctrl+c.
